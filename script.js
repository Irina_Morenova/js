function buttonClick(){
	var x1 = parseInt(document.getElementById('text_1').value);
	var x2 = parseInt(document.getElementById('text_2').value);

	var x1size = document.getElementById('text_1').size;
	var x2size = document.getElementById('text_2').size;

	if (Number.isNaN(x1) || Number.isNaN(x2) || x1size == 0 || x2size == 0){
		alert("Поля х1 и х2 должны быть заполнены числами");
	} else{
		var resultDiv = document.getElementById('result');
		var radio = document.getElementsByName('radioButton');
		if (radio[0].checked){
			resultDiv.innerHTML = "Сумма всех чисел от " + x1 + " до " + x2 + " = " + summing(x1, x2);
		} else if (radio[1].checked){	
			resultDiv.innerHTML = "Произведение всех чисел от " + x1 + " до " + x2 + " = " + multiplication(x1, x2);
		} else if (radio[2].checked){
			resultDiv.innerHTML = "Простые числа от " + x1 + " до " + x2 + ": " + prime(x1, x2);
		}
	}
}

function clearButton(){
	document.getElementById("text_1").value = "";
 	document.getElementById("text_2").value = "";
 	document.getElementById('result').innerHTML ="";
}

function summing(x1, x2){
	var summ = 0;
	for (var i = x1; i < x2+1; i++) {
		summ += i; 
	}
	return summ;
}

function multiplication(x1, x2){
	var mult = 1;
	for (var i = x1; i < x2+1; i++) {
		mult *= i; 
	}
	if (x1 > x2){
		mult = 0;
	}
	return mult;
}

function prime(x1, x2) {
	var prime;
	var ret = "";
	for (var i = x1; i < x2+1; i++) {
		prime = true;
		for (var j = 2; j < i; j++) {
			if (i%j == 0){
				prime = false;
			}
		}
		if (prime){
			ret += (i + " ");
		}
	}
	return ret;
}